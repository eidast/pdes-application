# -------------------------------------------------------------------
# Minimal dockerfile from alpine base
#
# Instructions:
# =============
# 1. Create an empty directory and copy this file into it.
#
# 2. Create image with: 
#   docker build --build-arg DB_USER=a_value --build-arg DB_PASSWORD=b_value --build-arg DB_NAME=c_value --tag timeoff:latest .
#
# 3. Run with: 
#	docker run -d -p 3000:3000 --name alpine_timeoff timeoff
#
# 4. Login to running container (to update config (vi config/app.json): 
#	docker exec -ti --user root alpine_timeoff /bin/sh
# --------------------------------------------------------------------
FROM alpine:3.8

ARG DB_USER=dbuser
ARG DB_PASSWORD=dbpassword
ARG DB_NAME=dbname

EXPOSE 3000

LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.docker.cmd="docker run -d -p 3000:3000 --name alpine_timeoff"

RUN apk add --no-cache \
    git \
    make \
    nodejs npm \
    python \
    vim \
    curl
    
RUN adduser --system app --home /app
USER app
WORKDIR /app
RUN git clone https://bitbucket.org/eidast/pdes-application.git timeoff-management
WORKDIR /app/timeoff-management

RUN npm install
RUN cat config/db.json | sed "s/DB_USER/$DB_USER/" | sed "s/DB_PASSWORD/$DB_PASSWORD/" | sed "s/DB_NAME/$DB_NAME/" > config/db_tmp.json && cp config/db_tmp.json config/db.json

CMD npm start