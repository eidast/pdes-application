/**
* TimeOff Jenkinsfile
* MAINTAINER: Alexander Moreno Delgado <eidast at gmail dot com>
*/
def APP_VERSION
def IMAGE_TAG
def GCP_WORKLOAD_NAME

pipeline {
    agent { label 'docker-build-slaves' }

    options {
        timestamps()
    }

    environment {
        SLAVE_BUILDER       = 'docker-build-slaves'
        
        BASE_MONOREPO_DIR   = '/'
        APP_NAME            = 'timeoff-application'
        GCP_PROJECT_NAME    = "${env.GCP_PROJECT_NAME}"

        COMMIT_HASH         = getCommitHash()
        COMMIT_SHORT        = COMMIT_HASH.substring(0,7)
        BRANCH_NAME         = "${env.GIT_BRANCH}"

        DB_USER             = "${env.DB_USERNAME}"
        DB_PASSWORD         = credentials('DB_PASSWORD')
        DB_NAME             = "${env.DB_NAME}"
    }
    stages {
        stage ("Notify Slack Start Execution") {
            steps {
                script {
                    slackMessage="*STARTED* - ${env.JOB_NAME} - *Build* ${env.BUILD_DISPLAY_NAME}. \n*Author:* ${env.CHANGE_AUTHOR_DISPLAY_NAME} \n*Commit Title:* ${env.CHANGE_TITLE} \n*Target:* ${env.CHANGE_TARGET} \n*Progress:* \n${env.BUILD_URL} \n*Console Output:* \n${env.BUILD_URL}console"
                    slackSend (channel: 'pdes-deployments', color: '#00FF00', message: slackMessage)
                }
            }
        }
        stage('Configuration retrieval') {
            steps {
                nodejs(nodeJSInstallationName: 'node_8_11_4') {
                    echo "GET NODE ?"
                    sh 'npm config ls'
                    sh 'npm --version'
                    sh 'node --version'
                    echo "GIT COMMIT TO PROCESS: ${COMMIT_HASH}"
                    echo "GIT BRANCH TO PROCESS: ${BRANCH_NAME}"
                }
                script {
                    GCP_WORKLOAD_NAME="timeoff-application"
                    IMAGE_TAG="gcr.io/${GCP_PROJECT_NAME}/${APP_NAME}:latest"
                }
            }
        }
        /**
        * --- BRANCH STAGE ---
        */
        stage('Building Branch Stages') {
            stages {
                stage('Building Docker Image') {
                    steps {
                            echo "Building Docker Image: ${IMAGE_TAG}"
                            sh "docker build --build-arg DB_USER=${DB_USER} --build-arg DB_PASSWORD=${DB_PASSWORD} --build-arg DB_NAME=${DB_NAME} -t ${IMAGE_TAG} ."
                            echo "Pushing Image to Docker Registry..."
                            sh "docker push ${IMAGE_TAG}"
                    }
                }
                stage('Deploying Image to Kubernetes DEVELOPMENT Cluster') {
                    steps {
                        echo "Deploying: ${IMAGE_TAG}"
                        sh "kubectl patch deployment ${APP_NAME} -p \"{\\\"spec\\\":{\\\"template\\\":{\\\"metadata\\\":{\\\"annotations\\\":{\\\"date\\\":\\\"`date +'%s'`\\\"}}}}}\""
                    }
                }
                stage('Deploy Healthcheck') {
                    steps {
                        timeout(time: 5, unit: 'MINUTES') {
                            retry(3) {
                                echo "Checking Deploy Health Check"
                                sh "kubectl rollout status deploy/${GCP_WORKLOAD_NAME} | grep successfully"
                            }
                        } 
                    }
                }
                stage('Registry Cleanup') {
                    steps {
                        sh "gcloud container images list-tags gcr.io/${GCP_PROJECT_NAME}/${APP_NAME} | tail -n +6 | awk '{print \$2}' | while read tag; do gcloud container images delete --force-delete-tags --quiet  gcr.io/${GCP_PROJECT_NAME}/${APP_NAME}:\$tag 2>&1 | tail; done;  " 
                    }
                }
            }
        }
    }
    post {
        success {
            script {
                sendStatusToSlack('#00FF00')
            }
        }
        unsuccessful {
            script {
                switch(currentBuild.currentResult.toUpperCase()) {
                    case "ABORTED":
                        color = "#FFDB58"
                        break
                    default:
                        color = '#FF0000'
                        break
                }
                sendStatusToSlack(color)
            }
        }
        cleanup {
            echo "- UNDEFINED CLEANUP STAGE -"
        }
    }
}

def sendStatusToSlack(color) {
    slackMessage="*${currentBuild.currentResult}* - ${env.JOB_NAME} - *Build* ${env.BUILD_DISPLAY_NAME}. \n*Console Output:* \n${env.BUILD_URL}console"
    slackSend (channel: 'pdes-deployments', color: color, message: slackMessage)
}

String getCommitHash() {
    sh "git log -1 | grep -i merge | wc -l > .git/is_merge"
    IS_MERGE = readFile(".git/is_merge").trim()
    if ( IS_MERGE != "0" ) {
        sh "git log --pretty=%H -2 | tail -1 > .git/actual_commit"
        COMMIT_HASH = readFile(".git/actual_commit").trim()
    }else{
        COMMIT_HASH = env.GIT_COMMIT
    }
    return COMMIT_HASH
}